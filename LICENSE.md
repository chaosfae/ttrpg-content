
© 2024 by chaosfae

The code for this website is marked with CC0 1.0. (https://creativecommons.org/publicdomain/zero/1.0/)

Other content, including but not limited to text in markdown, json data, images, fonts, and the result of rendering the code is *not* marked CC0. Some parts are derivative work of ORC Licensed Material and licensed to anyone who receives it under the same terms. Where that's the case, it's marked as such on the web site, and within the codebase if data is clearly derived from ORC Licensed Material, you can assume it can also be used under the terms of that license.
