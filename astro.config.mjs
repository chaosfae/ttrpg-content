import { defineConfig } from 'astro/config';
import remarkDirective from 'remark-directive';
import remarkDirectiveRehype from 'remark-directive-rehype';

import mdx from '@astrojs/mdx';
import styleDirectivePlugin from './src/util/styleDirectivePlugin';
import blockquoteAsAside from './src/util/blockquoteAsAside';

// https://astro.build/config
export default defineConfig({
	integrations: [mdx()],
	server: {
		port: 4323,
	},
	build: {
		format: 'preserve',
	},
	markdown: {
		remarkPlugins: [
			remarkDirective,
			styleDirectivePlugin,
			remarkDirectiveRehype,
		],
		remarkRehype: {
			handlers: {
				blockquote: blockquoteAsAside,
			},
		},
	},
});
