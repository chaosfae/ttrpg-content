import { append, curryN, lensProp, modifyPath, over, pipe, sort } from "ramda";
import bfrWeapons from "./bfr-weapons.json";
import type { BfrWeapon, BfrWeaponsTable } from "./types";

function getWeaponPath(name: string, data: BfrWeaponsTable): [string, number] {
	const list = Object.entries(data) as [keyof BfrWeaponsTable, BfrWeapon[]][];
	return (function iter(subset): [string, number] {
		if (subset.length === 0) {
			throw new Error(
				`Didn't find ${name}! List of weapons: ${list.flatMap(([, weapons]) =>
					weapons.map((weapon) => weapon.WEAPON)
				)}`
			);
		}
		const index = subset[0][1].findIndex((weapon) => weapon.WEAPON === name);
		if (index > -1) {
			return [subset[0][0], index];
		}
		return iter(subset.slice(1));
	})(list);
}

const addProperty = curryN(
	2,
	function addProperty(toAdd: string | string[], initial: string): string {
		const properties = initial.split(", ").concat(toAdd);
		properties.sort((a, b) => a.localeCompare(b));
		return properties.join(", ");
	}
);

const removeProperty = curryN(
	2,
	function addProperty(toRemove: string | string[], initial: string): string {
		const properties = initial.split(", ").filter((item) => item !== toRemove);
		properties.sort((a, b) => a.localeCompare(b));
		return properties.join(", ");
	}
);

function addThrownToSpear(data: BfrWeaponsTable): BfrWeaponsTable {
	const path = getWeaponPath("Spear", data);
	return modifyPath(
		path.concat("PROPERTIES"),
		addProperty("Thrown (range 10/60 ft.)"),
		data
	);
}

function removePullFromSpear(data: BfrWeaponsTable): BfrWeaponsTable {
	const path = getWeaponPath("Spear", data);
	return modifyPath(
		path.concat("WEAPON OPTION"),
		removeProperty("Pull"),
		data
	);
}

function addFinesseToGlaive(data: BfrWeaponsTable): BfrWeaponsTable {
	const path = getWeaponPath("Glaive", data);
	return modifyPath(
		path.concat("PROPERTIES"),
		addProperty("Finesse (1d8)"),
		data
	);
}

function addSabre(data: BfrWeaponsTable): BfrWeaponsTable {
	const sabre: BfrWeapon = {
		WEAPON: "Sabre",
		COST: "25 gp",
		DAMAGE: "1d8 slashing",
		WEIGHT: "3 lb.",
		"WEAPON OPTION": "Hamstring",
		PROPERTIES: "Finesse",
	};
	return over(
		lensProp<BfrWeaponsTable>("Martial Melee Weapons"),
		pipe(
			append(sabre),
			sort((a, b) => a.WEAPON.localeCompare(b.WEAPON))
		)
	)(data);
}

const modify = pipe(
	// prettier
	addThrownToSpear,
	removePullFromSpear,
	addFinesseToGlaive,
	addSabre
);

export default modify(bfrWeapons);
