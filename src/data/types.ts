export type BfrWeaponOption = {
  name: string;
  md: string;
};

export type BfrWeapon = {
  WEAPON: string;
  COST: string;
  DAMAGE: string;
  WEIGHT: string;
  "WEAPON OPTION": string;
  PROPERTIES: string;
};

export type BfrWeaponsTable = {
  "Simple Melee Weapons": BfrWeapon[];
  "Simple Ranged Weapons": BfrWeapon[];
  "Martial Melee Weapons": BfrWeapon[];
  "Martial Ranged Weapons": BfrWeapon[];
};
