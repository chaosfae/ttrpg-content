/// <reference types="mdast-util-directive" />
/// <reference types="remark-mdx" />

import clsx from 'clsx';
import type { Root } from 'mdast';
import type {
	ContainerDirective,
	LeafDirective,
	TextDirective,
} from 'mdast-util-directive';
import { visit } from 'unist-util-visit';

function addClass(
	className: string,
	node: ContainerDirective | LeafDirective | TextDirective,
) {
	if (node.attributes == null) {
		node.attributes = {};
	}
	node.attributes.class = clsx(node.attributes?.class, className);
}

export default function styleDirectivePlugin() {
	return (tree: Root) => {
		const pagesAndPageNumbers: string[] = [];

		visit(tree, node => {
			if (node.type === 'mdxJsxFlowElement' && node.name === 'PageNumber') {
				// @ts-expect-error
				pagesAndPageNumbers.push(node.attributes[0].value.value);
			}
			if (
				node.type === 'containerDirective' ||
				node.type === 'leafDirective' ||
				node.type === 'textDirective'
			) {
				if (node.name === 'page') {
					pagesAndPageNumbers.push('page');
				}
				if (node.name === 'page') {
					node.name = 'section';
					addClass('page', node);
				}
				if (node.name.startsWith('c-')) {
					const className = node.name.slice(2);
					node.name = 'div';
					addClass(className, node);
				}
			}
		});
		const pageCheck = pagesAndPageNumbers.reduce((collector, item) => {
			if (item === 'page') {
				collector.push(`Page ${collector.length + 1}; `);
			} else {
				if (collector.length === 0) {
					collector.push('Page ?; ');
				}
				collector[collector.length - 1] += `${item}.`;
			}
			return collector;
		}, [] as string[]);

		const error = !pageCheck.every((row, index) => {
			const correct = row === `Page ${index + 1}; ${index + 1}.`;
			return correct;
		});
		if (error) {
			console.error(`Expected numbers in order corrisponding to page, got:
${pageCheck.join('\n')}`);

			throw new Error(`Page numbers are wrong!`);
		}
	};
}
