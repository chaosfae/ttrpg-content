/// <reference types="mdast-util-directive" />

import { unified } from 'unified';
// import stream from 'unified-stream'
import remarkParse from 'remark-parse';
// import remarkToc from 'remark-toc'
import remarkRehype from 'remark-rehype';
// import rehypeFormat from 'rehype-format'
import rehypeStringify from 'rehype-stringify';
import remarkDirective from 'remark-directive';
import remarkDirectiveRehype from 'remark-directive-rehype';
import type { Root } from 'mdast';
import { visit } from 'unist-util-visit';

const allowedAttributes = [
	// prettier
	'class',
	'title',
];

function filterOutProperties() {
	return (tree: Root) => {
		visit(tree, node => {
			if (
				node.type === 'containerDirective' ||
				node.type === 'leafDirective' ||
				node.type === 'textDirective'
			) {
				// console.log(node);

				if (node.attributes == null) {
					return;
				}
				const attributes: Record<string, string | null | undefined> = {};
				Object.entries(node.attributes).forEach(([name, value]) => {
					if (allowedAttributes.includes(name) || name.startsWith('data-')) {
						attributes[name] = value;
					}
				});
				node.attributes = attributes;
			}
		});
	};
}

// remarkDirective enables
// https://talk.commonmark.org/t/generic-directives-plugins-syntax/444

const processor = unified()
	.use(remarkParse)
	// .use(remarkToc)
	.use(remarkDirective)
	.use(filterOutProperties)
	.use(remarkDirectiveRehype)
	.use(remarkRehype)
	// .use(rehypeFormat)
	.use(rehypeStringify);

export default async function md2html(markdown: string) {
	const output = await processor.process(markdown);
	return output;
}
