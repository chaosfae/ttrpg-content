import type { Blockquote } from 'mdast';
import type { State } from 'mdast-util-to-hast';
import type { Element } from 'hast';

export default function blockquoteAsAside(state: State, node: Blockquote): Element {
	const result: Element = {
		type: 'element',
		tagName: 'aside',
		properties: {},
		children: state.wrap(state.all(node), true),
	};
	state.patch(node, result);
	return state.applyData(node, result);
}
