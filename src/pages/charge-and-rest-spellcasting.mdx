---
layout: '../layouts/pages.astro'
title: 'Charge and Rest Spellcasting'
---

import CopyrightCCBYSA from '../components/CopyrightCCBYSA.astro';
import PageNumber from '../components/PageNumber.astro';
import '../page-styles/default.scss';
import '../page-styles/homebrew-tables.scss';

export function row(text) {
	return text.split(' ').map(td => <td>{td === '-' ? '' : td}</td>);
}
export function rows(content) {
	return content
		.split('\n')
		.map(text => text.trim())
		.filter(text => text !== '')
		.map(text => {
			return <tr>{row(text)}</tr>;
		});
}

::::::page

# Charge and Rest Spellcasting

:::::c-columns
::::c-column

Spellcaster levels now grant the ability to cast some leveled spells at-will, without expending a spell slot. This feature starts at your third caster level, where you gain second level spell slots, and at-will first level casting. However, all spells now require either a charge up or cool down time before or after casting them, equal to the spells level in rounds (6 seconds per spell level). Whether casting requires a charge up or cool down depends on your class, as outlined below.

| Class    | Requirement  |
| -------- | ------------ |
| Bard     | Charge up 🔥 |
| Cleric   | Charge up 🔥 |
| Druid    | Cool down 💧 |
| Paladin  | Charge up 🔥 |
| Ranger   | Cool down 💧 |
| Sorcerer | Charge up 🔥 |
| Warlock  | Cool down 💧 |
| Wizard   | Cool down 💧 |

Subclasses which grant spellcasting use the same casting type as the class from which they get their spells.
| Subclass | Requirement |
| - | - |
| Fighter (Eldritch Knight - Wizard) | Cool down 💧 |
| Rogue (Arcane Trickster - Wizard) | Cool down 💧 |
| Rogue (Soulspy - Cleric) | Charge up 🔥 |

## Charge up casting

These casters, exemplified by the sorcerer, need to take time to focus on their innate magic or divine prayers before each spell. They are immediately ready after casting to think about their next spell, and start charging up for it, which may only take one turn of not casting any leveled spells to then cast another 1st level spell.

## Cool down casting

Cool down casters, exemplified by the wizard, are able to nearly immediately translate their magical knowledge or gifts into casting a spell, but casting takes a toll on on the mind, and they need time after a spell to re-center and clear their mind, with that time depending on the level of the spell.

::::

::::c-column

## Pros and cons of each group

Charge up casters have very little initial opportunity cost, and can start nearly any encounter with their strongest spells immediately. Once in the fray, they must balance the cost of a small spell against the time it takes to get back to another more powerful burst.

Cool down casters have higher opportunity cost, and will often want to use their weaker, lower level spells first, holding their strongest tools for the middle or end of a fight, as they are more able to quickly pivot into their big spells. In the fray, they need to balance the cost of sending a big effect now against how long it will take before they can do any more leveled spells.

## Spellcasting level table

This table shows what spell levels you can cast at-will and how many spell slots you have for higher level spells.

<table>
	<thead>
		<tr>
			<th rowspan="2">Level</th>
			<th rowspan="2">
				At Will <br /> Spell Level
			</th>
			<th colspan="9">Spell Slots</th>
		</tr>
		<tr>
			<th>1st</th>
			<th>2nd</th>
			<th>3rd</th>
			<th>4th</th>
			<th>5th</th>
			<th>6th</th>
			<th>7th</th>
			<th>8th</th>
			<th>9th</th>
		</tr>
	</thead>
	<tbody>
		{rows(`
			1 0 2 - - - - - - - -
			2 0 3 - - - - - - - -
			3 1 ∞ 2 - - - - - - -
			4 1 ∞ 3 - - - - - - -
			5 1 ∞ 3 2 - - - - - -
			6 2 ∞ ∞ 3 - - - - - -
			7 2 ∞ ∞ 3 1 - - - - -
			8 2 ∞ ∞ 3 2 - - - - -
			9 3 ∞ ∞ ∞ 3 1 - - - -
			10 3 ∞ ∞ ∞ 3 2 - - - -
			11 3 ∞ ∞ ∞ 3 2 1 - - -
			12 4 ∞ ∞ ∞ ∞ 2 1 - - -
			13 4 ∞ ∞ ∞ ∞ 2 1 1 - -
			14 4 ∞ ∞ ∞ ∞ 2 1 1 - -
			15 4 ∞ ∞ ∞ ∞ 2 1 1 1 -
			16 4 ∞ ∞ ∞ ∞ 2 1 1 1 -
			17 4 ∞ ∞ ∞ ∞ 2 1 1 1 1
			18 5 ∞ ∞ ∞ ∞ ∞ 1 1 1 1
			19 6 ∞ ∞ ∞ ∞ ∞ ∞ 1 1 1
			20 7 ∞ ∞ ∞ ∞ ∞ ∞ ∞ 1 1
		`)}
	</tbody>
</table>

::::

:::::

<PageNumber number={1} />
::::::

::::::page
:::::c-columns
::::c-column

## Multiclassing

Charge or cool casting does not limit your multiclassing options, and you calculate your total spellcasting level the usual way, before referencing the new spellcasting level table. A side effect of this is that when you multiclasses, if you are unable to learn or prepare spells at the maximum slot level you have, you can end up having only spells in slot sizes that you can cast infinitely. You can still upcast your spells to use the limited higher slots.

If you multiclass into both charge up and cool down caster classes, or otherwise know both charge up and cool down spells, the charge up and cool down times do interact. If you wish to cast a 5th level charge up spell, you must not cast any spell, including cool down spells, for the 5 rounds beforehand. Likewise, if you cast a 5th level cool down spell, you can not cast any spell, including charge up spells, for the next 5 rounds. Additionally, if you cast a cool down spell, the cool down rounds can not count toward charge up rounds. For example the minimum time between casting a 3rd level cool down spell and then a 2nd level charge up spell is 5 rounds where you do not cast any leveled spells. On the flip side, you could in reverse begin by charging up 2 rounds, then cast the 2nd level charge up spell, then the next round cast a 3rd level cool down spell (and cast no spells the next 3 rounds during the cool down).

::::
::::c-column

<CopyrightCCBYSA bfr srd5e workName="Charge And Rest Spellcasting" year={2024}>
<div slot="after-header">
Background texture modified from original art by Eren Angiolini.

</div>
</CopyrightCCBYSA>
::::

:::::

<PageNumber number={2} />
::::::
