---
layout: '../../layouts/article.astro'
title: 'Challenge Recipe'
published: 2024-11-28
---
# Three Stage Dungeon

How to make challenges interesting, a short recipe

Create three types of traps/challenges. Don't make them complex. Each challenge should have something that visibly identifies it so players can recognize it again later. Interactions with them should consume minimal resources besides actions. (Players will expend resources on the adds within the stages.)

> What are "adds"? Adds are the lower level enemies in an encounter that exist to keep players on their toes and provide extra challenge. Usually they are also "minions" and frequently are very easy to defeat.

Have three stages for the dungeon, with sections between.

**Stage 1.** In the first stage, introduce the first trap/challenge. Don't make it particularly deadly. Let the players figure it out with minimal losses.

Between the first and second stage, players can interact with small rehashed versions of the stage 1 challenge to find extras.

**Stage 2.** In the second stage, either just introduce the second trap/challenge, or have it added to the stage one challenge as well. It should be a distinct new mechanic. With both, the new mechanic should be the main event.

Between stage 2 and 3, allow for some more small treats where players can exploit their knowledge of how the challenges works to get small prizes.

**Stage 3.** In the last stage, introduce the third challenge on top of the stage 1 and 2 mechanics. They should all be tied together, but at least some of the time you'd want players to be able to handle the different parts separately. This encounter should take the longest, but the stage 1 and 2 mechanics should be set up to not require as much time as they did in their specific stages.

Each stage includes adds, but at least one should also include a "boss" of some kind. Having someone you build up to is a good approach, such as fighting only minions in the first stage, then a lieutenant and minions in the second, and finally the big bad of the dungeon in the third stage. Having the big bad loom from inaccessible places is a fun choice for the first couple stages.